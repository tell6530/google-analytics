using System.IO;
using System.Threading;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Microsoft.Extensions.Configuration;

namespace google.analytics.BL
{
    public class GAManagementService : GABaseService, IGAManagementService
    {
        protected override string[] Scopes
        {
            get
            {
                return new string[] 
                        { 
                            AnalyticsService.Scope.AnalyticsManageUsers
                            , AnalyticsService.Scope.Analytics
                            , AnalyticsService.Scope.AnalyticsEdit
                            , AnalyticsService.Scope.AnalyticsProvision
                            , AnalyticsService.Scope.AnalyticsUserDeletion
                            // , AnalyticsService.Scope.AnalyticsReadonly
                            // , AnalyticsService.Scope.AnalyticsManageUsersReadonly
                        };
            }
        }

        private IConfiguration _configuration;

        private AnalyticsService _analyticsService;

        public GAManagementService(IConfiguration configuration)
        {
            this._configuration = configuration;

            //// OAuth
            // var oauthCredentialFilePath = this._configuration.GetSection("OAuthDesktopCredentialPath").Value;
            // var oauthCredential = this.GetOAuthCredential(oauthCredentialFilePath);
            // this._analyticsService = new AnalyticsService(new BaseClientService.Initializer(){
            //     HttpClientInitializer = oauthCredential,
            //     ApplicationName = "GA Project"
            // });

            //// Service Account
            var serviceAccountCredentialFilePath = this._configuration.GetSection("CredentialPath").Value;
            var serviceAccountCredential = this.GetServiceAccountCredential(serviceAccountCredentialFilePath);
            this._analyticsService = new AnalyticsService(new BaseClientService.Initializer(){
                HttpClientInitializer = serviceAccountCredential,
                ApplicationName = "GA Project"
            });
        }

        public Accounts GetAccounts()
        {
            var request = this._analyticsService.Management.Accounts.List();

            var requestResult = request.Execute();

            return requestResult;
        }

        public Webproperties GetWebproperties()
        {
            var accountId = "188872785";
            var request = this._analyticsService.Management.Webproperties.List(accountId);

            var requestResult = request.Execute();

            return requestResult;
        }

        public void CreateAccountTree()
        {
            var requestBody = new AccountTreeRequest()
            {
                AccountName = "susuAPIAccount",
                Kind = "analytics#account",
                ProfileName = "susuAPIProfile",
                WebpropertyName = "susuWebproperty",
                WebsiteUrl = "www.google.com",
                Timezone = "Asia/Taipei"
            };

            var request = this._analyticsService.Provisioning.CreateAccountTree(requestBody);

            request.Execute();
        }
    }
}