using Google.Apis.Services;
using Google.Apis.GoogleAnalyticsAdmin.v1alpha;
using Google.Apis.GoogleAnalyticsAdmin.v1alpha.Data;
using Microsoft.Extensions.Configuration;

namespace google.analytics.BL
{
    public class GAAdminService : GABaseService, IGAAdminService
    {
        protected override string[] Scopes
        {
            get
            {
                return new string[] 
                        { 
                            GoogleAnalyticsAdminService.Scope.AnalyticsEdit
                            , GoogleAnalyticsAdminService.Scope.AnalyticsManageUsers
                            // , GoogleAnalyticsAdminService.Scope.AnalyticsManageUsersReadonly
                            // , GoogleAnalyticsAdminService.Scope.AnalyticsReadonly
                        };
            }
        }

        private GoogleAnalyticsAdminService _gaAdminService;

        private IConfiguration _configuration;

        public GAAdminService(IConfiguration configuration)
        {
            this._configuration = configuration;

            //// OAuth
            var oauthCredentialFilePath = this._configuration.GetSection("OAuthDesktopCredentialPath").Value;
            var oauthCredential = this.GetOAuthCredential(oauthCredentialFilePath);
            this._gaAdminService = new GoogleAnalyticsAdminService(new BaseClientService.Initializer(){
                HttpClientInitializer = oauthCredential,
                ApplicationName = "GA Project"
            });

            //// Service Account
            // var serviceAccountCredentialFilePath = this._configuration.GetSection("CredentialPath").Value;
            // var serviceAccountCredential = this.GetServiceAccountCredential(serviceAccountCredentialFilePath);
            // this._gaAdminService = new GoogleAnalyticsAdminService(new BaseClientService.Initializer(){
            //     HttpClientInitializer = serviceAccountCredential,
            //     ApplicationName = "GA Project"
            // });
        }

        public GoogleAnalyticsAdminV1alphaProvisionAccountTicketResponse CreateAccount()
        {
            var accountRequestEntity = new GoogleAnalyticsAdminV1alphaProvisionAccountTicketRequest()
            {
                Account = new GoogleAnalyticsAdminV1alphaAccount(){
                    DisplayName = "ByAPI",
                    RegionCode = "TW"
                    // ETag = null
                },
                RedirectUri = "http://localhost",
                // ETag = "BYCh9sUlhCDIy2msn_S_jLNXKqeprc32Kn7hypHl0Ts/OANMjYB6P86qwYMjdXhd8Z_O8_I"
            };

            var request = this._gaAdminService.Accounts.ProvisionAccountTicket(accountRequestEntity);

            return request.Execute();
        }
    }
}