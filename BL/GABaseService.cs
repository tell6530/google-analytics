using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;

namespace google.analytics.BL
{
    public abstract class GABaseService
    {
        protected abstract string[] Scopes { get;}

        protected virtual UserCredential GetOAuthCredential(string credentialFilePath)
        {
            using (var stream =
                new FileStream(credentialFilePath, FileMode.Open, FileAccess.Read))
            {
                var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        this.Scopes,
                        "susu oauth test",
                        CancellationToken.None).Result;

                return credential;
            }
        }

        protected virtual GoogleCredential GetServiceAccountCredential(string credentialFilePath)
        {
            using (var stream =
                new FileStream(credentialFilePath, FileMode.Open, FileAccess.Read))
            {
                var credential = GoogleCredential.FromStream(stream)
                                                 .CreateScoped(this.Scopes);

                return credential;
            }
        }
    }
}