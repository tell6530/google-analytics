using Google.Apis.Analytics.v3.Data;

namespace google.analytics.BL
{
    public interface IGAManagementService
    {
         Accounts GetAccounts();

         Webproperties GetWebproperties();

         void CreateAccountTree();
    }
}