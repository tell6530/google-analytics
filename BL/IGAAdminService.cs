using Google.Apis.GoogleAnalyticsAdmin.v1alpha.Data;

namespace google.analytics.BL
{
    public interface IGAAdminService
    {
         GoogleAnalyticsAdminV1alphaProvisionAccountTicketResponse CreateAccount();
    }
}