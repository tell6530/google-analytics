﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using google.analytics.BL;
using System.Net;
using System.Threading;

namespace google.analytics
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostContext, configuration) =>
                {
                    configuration.SetBasePath(Directory.GetCurrentDirectory())
                                 .AddEnvironmentVariables()
                                 .AddJsonFile(path: "AppSettings.json", optional: true, reloadOnChange: true);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<IHostedService, WorkerProcess>();
                    services.AddSingleton<IGAManagementService, GAManagementService>();
                    services.AddSingleton<IGAAdminService, GAAdminService>();
                });
    }
}
