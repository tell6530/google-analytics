using Newtonsoft.Json;

namespace google.analytics.BE
{
    /// <summary>
    /// GAManagementServiceAccountCredential
    /// </summary>
    /// <remarks>
    /// https://console.developers.google.com/apis/credentials
    /// </remarks>
    public class GAManagementServiceAccountCredential
    {
        /// <summary>
        /// ProjectId
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// ProjectId
        /// </summary>
        [JsonProperty("project_id")]
        public string ProjectId { get; set; }

        /// <summary>
        /// PrivateKeyId
        /// </summary>
        [JsonProperty("private_key_id")]
        public string PrivateKeyId { get; set; }

        /// <summary>
        /// PrivateKey
        /// </summary>
        [JsonProperty("private_key")]
        public string PrivateKey { get; set; }

        /// <summary>
        /// ClientEmail
        /// </summary>
        [JsonProperty("client_email")]
        public string ClientEmail { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        /// <summary>
        /// AuthUri
        /// </summary>
        [JsonProperty("auth_uri")]
        public string AuthUri { get; set; }

        /// <summary>
        /// TokenUri
        /// </summary>
        [JsonProperty("token_uri")]
        public string TokenUri { get; set; }

        /// <summary>
        /// AuthProviderX509CertUrl
        /// </summary>
        [JsonProperty("auth_provider_x509_cert_url")]
        public string AuthProviderX509CertUrl { get; set; }

        /// <summary>
        /// ClientX509CertUrl
        /// </summary>
        [JsonProperty("client_x509_cert_url")]
        public string ClientX509CertUrl { get; set; }
    }
}