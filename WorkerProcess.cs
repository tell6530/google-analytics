using System;
using System.Threading;
using System.Threading.Tasks;
using google.analytics.BL;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

namespace google.analytics
{
    public class WorkerProcess : BackgroundService
    {
        private IGAManagementService _gaManagementService;

        private IGAAdminService _gaAdminService;

        private IConfiguration _configuration;

        public WorkerProcess(
            IGAManagementService gaManagementService
            , IGAAdminService gAAdminService
            , IConfiguration configuration)
        {
            this._gaManagementService = gaManagementService;
            this._gaAdminService = gAAdminService;
            this._configuration = configuration;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(() =>
            {
                var createAccount = this._gaAdminService.CreateAccount();

                var accouts = this._gaManagementService.GetAccounts();

                // var webProperties = this._gaManagementService.GetWebproperties();

                // this._gaManagementService.CreateAccountTree();
            });
        }
    }
}